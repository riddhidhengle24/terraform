# #creating variable ubuntu instance

# provider "aws" {
#     region = var.region
#     shared_config_files      = ["/root/.aws/config"]
#     shared_credentials_files = ["/root/.aws/credentials"]
#     profile                  = var.profile
# }

# #data block for SG
# data "aws_security_group" "my_sg" {
#   name = "data"
# }

# resource "aws_instance" "my_instance3" {
#   ami = var.ami
#   instance_type = var.instance_type
#   key_name = var.key_name
#   vpc_security_group_ids = [data.aws_security_group.my_sg.id]
#   tags = var.tags
# }

# variable "region" {
#     type = string
#     description = "region variable"
#     default = "us-west-2"
# }

# variable "profile" {
#     type = string
#     description = "profile variable"
#     default = "riddhi"
# }

# variable "ami" {
#     type = string
#     description = "ami id variable"
#     default = "ami-08f7912c15ca96832"
# }

# variable "instance_type" {
#     type = string
#     description = "instance_type variable"
#     default = "t2.micro"
# }

# variable "key_name" {
#     type = string
#     description = "key name variable"
#     default = "windows"
# }

# variable "tags" {
#     type = map
#     description = "profile variable"
#     default = {
#         Name = "var_instance"
#         name = "xyz"
#         environment = "production"
#         project = "abc"
#     }
# }

# output "created_server" {
#     value = data.aws_security_group.my_sg
# }