# #creating vpc

# provider "aws" {
#     region = "us-west-2"
#     shared_config_files      = ["/root/.aws/config"]
#     shared_credentials_files = ["/root/.aws/credentials"]
#       profile                  = "riddhi"
# }

# resource "aws_vpc" "main" {
#   cidr_block = "192.168.0.0/16"
#   tags = {
#     Name = "main_vpc"
#   }
# }

# resource "aws_subnet" "main" {
#   vpc_id     = aws_vpc.main.id
#   cidr_block = "192.168.16.0/20"

#   tags = {
#     Name = "Main"
#   }
# }

# resource "aws_internet_gateway" "gw" {
#   vpc_id = aws_vpc.main.id

#   tags = {
#     Name = "main"
#   }
# }

# resource "aws_route_table" "example" {
#   vpc_id = aws_vpc.main.id
#   route {
#     cidr_block = "10.0.1.0/24"
#     gateway_id = aws_internet_gateway.gw.id
#   }

# }

# resource "aws_route_table_association" "a" {
#   subnet_id      = aws_subnet.main.id
#   route_table_id = aws_route_table.example.id
# }

# resource "aws_route_table_association" "b" {
#   gateway_id = aws_internet_gateway.gw.id
#   route_table_id = aws_route_table.example.id
# }

#   resource "aws_internet_gateway_attachment" "example" {
#   internet_gateway_id = aws_internet_gateway.gw.id
#   vpc_id              = aws_vpc.main.id
# }

# output "created_server" {
#     value = "yes"
# }