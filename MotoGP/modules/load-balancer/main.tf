# # Create a new load balancer
# resource "aws_elb" "clb" {
#   name               = "CLB"
#   internal = false
#   subnets = var.subnet
#   availability_zones = var.availability_zones

#   listener {
#     instance_port     = 80
#     instance_protocol = "http"
#     lb_port           = 80
#     lb_protocol       = "http"
#   }

#   health_check {
#     healthy_threshold   = 2
#     unhealthy_threshold = 2
#     timeout             = 5
#     target              = "HTTP:80/"
#     interval            = 30
#   }

#   instances                   = ["aws_instance.instance1.id"]
#   cross_zone_load_balancing   = true
#   idle_timeout                = 40
#   connection_draining         = true
#   connection_draining_timeout = 40

#   tags = {
#     Name = "CLB"
#   }
# }