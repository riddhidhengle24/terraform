resource "aws_instance" "instance1" {
  count = var.instance_count
  ami = var.ami
  instance_type = var.instance_type
  key_name = var.key_name
  tags = var.tags
  subnet_id = var.subnet_id
  associate_public_ip_address = true

  user_data = <<-EOF
  #!/bin/bash
  sudo -i
  apt update -y
  apt install nginx -y
  echo "Welcome $HOSTNAME" > /var/www/html/index.nginx-debian.html
  systemctl start nginx
  systemctl enable nginx
  EOF
}