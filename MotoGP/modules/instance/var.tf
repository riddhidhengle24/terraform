variable "instance_count" {}

variable "ami" {}

variable "instance_type" {}

variable "key_name" {}

variable "tags" {}

variable "subnet_id" {}