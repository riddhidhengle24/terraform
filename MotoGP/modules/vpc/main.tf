#create vpc
resource "aws_vpc" "my_vpc1" {
  cidr_block = var.cidr_block
  tags = {
    Name = "myvpc"
    name = "pqr"
    Environment = "prod"
  }
}

#create subnet
resource "aws_subnet" "subnet" {
  vpc_id = aws_vpc.my_vpc1.id
  cidr_block = var.subnet
  availability_zone = "us-west-2a"
  tags = {
    Name = "my_subnet"
  }
}

#create internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.my_vpc1.id
  tags = {
    Name = "myigw"
  }
}

#To attach IGW to vpc
resource "aws_internet_gateway_attachment" "myigw" {
  vpc_id = aws_vpc.my_vpc1.id
  internet_gateway_id = aws_internet_gateway.igw.id
}

#to identify default route table of created vpc
data "aws_route_table" "my-route" {
  vpc_id = aws_vpc.my_vpc1.id
}

#create route in default route table to point IGW
resource "aws_route" "default" {
  route_table_id = data.aws_route_table.my-route.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.igw.id
}
