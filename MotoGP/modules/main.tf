provider "aws" {
  region = var.region
  profile = var.profile
}

terraform {
  backend "s3" {
    bucket = "terraform-tfstate-b25"
    key = "terraform.tfstate"
    region = "us-west-2"
    dynamodb_table = "terraform-table"
  }
}

# module "clb" {
#   source = "./load-balancer"
#   availability_zones = var.availability_zones
#   subnet = var.subnet
# }

module "instance1" {
  source = "./instance/"
  instance_count = var.instance_count
  ami = var.ami
  instance_type = var.instance_type
  key_name = var.key_name   
  tags = var.tags
  subnet_id = module.my_vpc1.subnet_id
}

module "my_vpc1" {
  source = "./vpc"
  cidr_block = var.cidr_block
  subnet = var.subnet
}

