variable "region" {
  default = "us-west-2"
}

variable "profile" {
  default = "riddhi"
}

variable "availability_zones" {
  type = set(string)
  default = ["us-west-2a", "us-west-2b"] 
}

variable "instance_count" {
default = 2
}

variable "ami" {
    default = "ami-08f7912c15ca96832"  
}

variable "instance_type" {
    default = "t2.micro"
}

variable "key_name" { 
default = "windows"
}

variable "tags" {
default = {
        Name = "var_instance"
        name = "xyz"
        Environment = "production"
        project = "abc"
}
}

variable "cidr_block" {
  default = "192.168.0.0/18"
}

variable "subnet" {
  default = "192.168.0.0/22"
}